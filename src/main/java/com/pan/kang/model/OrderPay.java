package com.pan.kang.model;

import com.pan.kang.support.PayMoneyValid;
import com.pan.kang.support.PayStyleGroupSequenceProvider;
import lombok.Data;
import org.hibernate.validator.group.GroupSequenceProvider;


import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;

@Data
@GroupSequenceProvider(PayStyleGroupSequenceProvider.class)
public class OrderPay {

    private String id;

    private String payStyle;

    @NotBlank(groups = PayStyle.class, message = "支付方式不为空，openId 必传！！！")
    private String openId;

    private Date payTime;

    @PayMoneyValid
    private BigDecimal payMoney;

    @PayMoneyValid(message = "支付金额必须大于10！！！", money = 10.00)
    private BigDecimal payment;

    public interface PayStyle {
    }


}
