package com.pan.kang.controller;

import com.pan.kang.model.OrderPay;
import com.pan.kang.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/pay")
public class PayController {

    @Autowired
    private PayService payService;

    @PostMapping("/V1.0/payCallback")
    public Boolean payCallback(@Valid @RequestBody OrderPay orderPay) {
        log.info("payCallback 请求参数：{}", orderPay);
        return true;
    }


    @PostMapping("/V1.0/pay")
    public Boolean pay(@Valid @RequestBody OrderPay orderPay) {
        log.info("pay 请求参数：{}", orderPay);
        return payService.pay(orderPay);
    }
}
