package com.pan.kang.support;


import com.pan.kang.model.OrderPay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.function.Predicate;

@Slf4j
public class OrderPayValidation<T extends Annotation> implements ConstraintValidator<T, OrderPay> {

    protected Predicate<OrderPay> condition = orderPay -> true;

    @Override
    public boolean isValid(OrderPay orderPay, ConstraintValidatorContext constraintValidatorContext) {
        return orderPay == null || condition.test(orderPay);
    }

    public static class CheckPayStyleValidator extends OrderPayValidation<PayStyleValid> {
        @Override
        public void initialize(PayStyleValid constraintAnnotation) {
            condition = orderPay -> {
                String payStyle = orderPay.getPayStyle();
                return StringUtils.isEmpty(payStyle) || "微信".equals(payStyle)
                        || "支付宝".equals(payStyle);
            };
        }
    }
}


