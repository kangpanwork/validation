package com.pan.kang.support;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.function.Predicate;

public class PayMoneyValidator implements ConstraintValidator<PayMoneyValid, BigDecimal> {

    protected Predicate<BigDecimal> condition = money -> true;

    @Override
    public void initialize(PayMoneyValid constraintAnnotation) {
        BigDecimal money = BigDecimal.valueOf(constraintAnnotation.money());
        condition = m -> m != null && m.compareTo(money) > 0;
    }

    @Override
    public boolean isValid(BigDecimal money, ConstraintValidatorContext constraintValidatorContext) {
        return condition.test(money);
    }
}
