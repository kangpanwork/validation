package com.pan.kang.support;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


import static java.lang.annotation.ElementType.*;


@Target({PARAMETER, FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PayMoneyValidator.class})
@Documented
public @interface PayMoneyValid {

    double money() default 100.00;

    String message() default "支付金额必须大于 100 元！";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
