package com.pan.kang.support;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({TYPE, ANNOTATION_TYPE, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {OrderPayValidation.CheckPayStyleValidator.class})
@Documented
public @interface PayStyleValid {

    String message() default "支付方式不是支付宝或者微信！！！";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
