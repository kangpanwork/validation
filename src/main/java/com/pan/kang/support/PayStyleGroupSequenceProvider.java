package com.pan.kang.support;

import com.pan.kang.model.OrderPay;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;


public class PayStyleGroupSequenceProvider implements DefaultGroupSequenceProvider<OrderPay> {

    @Override
    public List<Class<?>> getValidationGroups(OrderPay orderPay) {
        List<Class<?>> list = new LinkedList<>();
        // 这一步不能省,否则 Default 分组都不会执行了，会报错
        list.add(OrderPay.class);
        // 这块判空请务必要做
        if (orderPay != null) {
            String payStyle = orderPay.getPayStyle();
            if (!StringUtils.isEmpty(payStyle)) {
                list.add(OrderPay.PayStyle.class);
            }
        }
        return list;
    }
}
