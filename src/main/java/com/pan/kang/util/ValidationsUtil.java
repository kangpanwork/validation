package com.pan.kang.util;


import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class ValidationsUtil {

    private ValidationsUtil() {
    }

    public static void isNullThanThrow(final Object args, final String message) {
        check(Objects.isNull(args), message);
    }

    public static void isEmptyThanThrow(final String args, final String message) {
        check(StringUtils.isEmpty(args), message);
    }

    public static void isEmptyThanThrow(final Collection<?> args, final String message) {
        check(CollectionUtils.isEmpty(args), message);
    }

    public static void isEmptyThanThrow(final Map<?, ?> args, final String message) {
        check(CollectionUtils.isEmpty(args), message);
    }

    public static void check(final boolean expression, final String message) {
        BooleanUtils.isAllMatch(expression).handleCondition(() -> {
            throw new RuntimeException(message);
        });
    }
}
