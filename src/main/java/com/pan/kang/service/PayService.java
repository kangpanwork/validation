package com.pan.kang.service;

import com.pan.kang.model.OrderPay;
import com.pan.kang.support.PayStyleValid;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface PayService {
    boolean pay(@Valid @PayStyleValid OrderPay orderPay);
}
